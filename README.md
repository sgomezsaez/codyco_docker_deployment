# PROVISIONING AND DEPLOYMENT - CODYCO INFRASTRUCTURE

## 1. REQUIREMENTS

	* Ubuntu >= 14.04 64 bit

	* Docker >= 1.2.0
	

## 2. INFRASTRUCTURE SETUP

	2.1. Enable execution permission: `chmod +x provision_containers.sh`

	2.2. Configure parameters in `./provision_containers.sh`
	2.3. Provision containers: `./provision_containers.sh`
	
	Note: the scripts will obtain the IP address exposed in the interface eth0 and expose the services/servers on it. 

## 3. ECLIPSE INSTALLATION
	* Download CoDyCo Eclipse Moderler from http://www.iaas.uni-stuttgart.de/forschung/projects/CoDyCo/
	
		OR

	* Download and install Eclipse Helios Java EE

	* Download and install eclipse plugins 

		- URL [Codyco Update Site]: (http://kaaroda.informatik.uni-stuttgart.de:8080/userContent/codyco-update-site/)

## 4. ECLIPSE CONFIGURATION

	* Configure Eclipse

		- Connection to ActiveMQ: 

			CoDyCo Modeler -> Preferences -> CoDyCo Modeler

			URL: tcp://<ip_address>:61616

		- Connection to Fragmento: 

			Fragmento Settings

			URL: http://<ip_address>:8050/Repository/services/FragmentServiceImplService.FragmentServicePort?wsdl

 		- Connection to ODE

			CoDyCo Modeler -> Preferences -> ODE

			URL: http://<ip_address>:8080/ode/
