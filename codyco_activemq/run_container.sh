#!/bin/bash

echo "Running container ActiveMQ"

IPADD=$(ifconfig eth0 | grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}')

sudo docker run -d -p $IPADD:$ACTIVEMQ_MANAGEMENT_PORT1:$ACTIVEMQ_MANAGEMENT_PORT1 -p $IPADD:$ACTIVEMQ_MANAGEMENT_PORT2:$ACTIVEMQ_MANAGEMENT_PORT2 -p $IPADD:$ACTIVEMQ_JMS_BROKER_PORT:$ACTIVEMQ_JMS_BROKER_PORT -p $IPADD:$ACTIVEMQ_ADMIN_PORT:$ACTIVEMQ_ADMIN_PORT -net host -t codyco/activemq
