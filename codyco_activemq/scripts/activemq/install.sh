#!/bin/bash

#DOWNLOAD_URL="http://ftp-stud.hs-esslingen.de/pub/Mirrors/ftp.apache.org/dist/activemq/5.9.1/apache-activemq-5.9.1-bin.tar.gz"
set -e

DOWNLOAD_URL=${ACTIVEMQ_DOWNLOAD_URL}

echo "Installing ActiveMQ $ACTIVEMQ_VERSION..."

# Download and extract ActiveMQ bundle
echo "Checking if package already contained in $ACTIVEMQ_PACKAGE ..."

amqpackage=$ACTIVEMQ_PACKAGE/apache-activemq-$ACTIVEMQ_VERSION-bin.tar.gz

echo "Checking if file $amq_package exists..."

if [ -d "$amqpackage" ]; then
	cp -R $amqpackage /usr/local/apache-activemq-$ACTIVEMQ_VERSION
	#cp -R /usr/local/apache-activemq-$ACTIVEMQ_VERSION/apache-activemq-$ACTIVEMQ_VERSION /usr/local/
	#rm -R /usr/local/apache-activemq-$ACTIVEMQ_VERSION/apache-activemq-$ACTIVEMQ_VERSION/
else
	echo "File $amq_package does not exist. Downloading from $DOWNLOAD_URL"
	wget $DOWNLOAD_URL
	tar -xvf apache-activemq-$ACTIVEMQ_VERSION-bin.tar.gz
	cp -R apache-activemq-$ACTIVEMQ_VERSION /usr/local/apache-activemq-$ACTIVEMQ_VERSION
	rm -R apache-activemq-$ACTIVEMQ_VERSION
fi


# Make the activemq script executable
chmod 755 $ACTIVEMQ_HOME/bin/activemq

#sed -i "s/openwire/http/" $ACTIVEMQ_HOME/conf/activemq.xml
#sed -i "s/tcp/http/" $ACTIVEMQ_HOME/conf/activemq.xml


# Create a deamon for activemq
cp $ACTIVEMQ_SCRIPTS/start.sh $ACTIVEMQ_HOME/
#cp activemq/status.sh $AMQ_HOME/
cp $ACTIVEMQ_SCRIPTS/stop.sh $ACTIVEMQ_HOME/

mkdir $ACTIVEMQ_HOME/tmp
#chown activemq:activemq $AMQ_HOME/tmp
