#!/bin/bash
export TOMCAT_PACKAGES=/packages/tomcat
export TOMCAT_SCRIPTS=/scripts/tomcat
#export TOMCAT_USER=allow
#export TOMCAT_PASSWORD=allow
export FRAGMENTO_DB_NAME=repository
export TOMCAT_HOME=/opt/apache-tomcat-7.0.54
#export TOMCAT_PORT=8050
export AUDITING_PACKAGES=/packages/auditing
export ENGINE_PACKAGES=/packages/engine
export MYSQL_DRIVERS=/packages/mysql
export CATALINA_HOME=/opt/apache-tomcat-7.0.54
#export JRE_HOME=$JAVA_JRE
export ENGINE_SCRIPTS=/scripts/engine
export AUDITING_SCRIPTS=/scripts/auditing
export ACTIVEMQ_SCRIPTS=/scripts/activemq


