#!/bin/bash

echo "Configuring shared process directory..."

while [ ! -d "/opt/apache-tomcat-7.0.54/webapps/ode/WEB-INF/processes"  ] 
do

echo "Directory /opt/apache-tomcat-7.0.54/webapps/ode/WEB-INF/processes not yet created..."
sleep 1

done


if [ ! -f "/opt/processes/README.txt" ]; then

	echo "file /opt/processes/README.txt, moving the files and creating sym link"
	mv /opt/apache-tomcat-7.0.54/webapps/ode/WEB-INF/processes/* /opt/processes/
	rm -R /opt/apache-tomcat-7.0.54/webapps/ode/WEB-INF/processes
	ln -s /opt/processes  /opt/apache-tomcat-7.0.54/webapps/ode/WEB-INF/processes
	#ln -s /opt/apache-tomcat-7.0.54/webapps/ode/WEB-INF/processes /opt/processes
fi

#chmod -R a+rw /opt/apache-tomcat-7.0.54/webapps/ode/WEB-INF/processes/
#chmod a+rw /opt/processes/processes


