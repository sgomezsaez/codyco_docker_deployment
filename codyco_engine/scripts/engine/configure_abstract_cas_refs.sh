#!/bin/bash

set -e

echo "Configuring CoDyCo Abstract CAS external references:"
echo "Fragment Repository URL: $FRAGMENT_WEB_SERVICE_REPO_URL"
echo "Adaptation Manager URL: $ADAPTATION_MANAGER_WEB_SERVICE_URL"

WAR_PKG=$ODE_WAR_PACKAGE

echo "Extracting ODE WAR package "
mkdir -p $ENGINE_PACKAGES/tmp
cp $ENGINE_PACKAGES/apache-ode-war/$WAR_PKG $ENGINE_PACKAGES/tmp/$WAR_PKG

echo "Configuring Engine package to connect to external Abstract CAS components ..."

unzip $ENGINE_PACKAGES/tmp/$WAR_PKG -d $ENGINE_PACKAGES/tmp/ > /dev/null 2>&1


if [[ ! -f "$ENGINE_PACKAGES/tmp/WEB-INF/conf/ode-axis2.properties" ]]; then
        echo "File ode-axis2.properties, copying template configuration..."
        cp $ENGINE_CONFIG/ode-axis2.properties  $ENGINE_PACKAGES/tmp/WEB-INF/conf/ode-axis2.properties
fi

if [[ ! -f "$ENGINE_PACKAGES/tmp/WEB-INF/conf/static-fragmento.properties" ]]; then
        echo "static-fragmento.properties, copying template configuration..."
        cp $ENGINE_CONFIG/static-fragmento.properties  $ENGINE_PACKAGES/tmp/WEB-INF/conf/static-fragmento.properties
fi

if [[ ! -f "$ENGINE_PACKAGES/tmp/WEB-INF/conf/adaptation.properties" ]]; then
        echo "adaptation.properties, copying template configuration..."
        cp $ENGINE_CONFIG/adaptation.properties  $ENGINE_PACKAGES/tmp/WEB-INF/conf/adaptation.properties
fi

#cp $ENGINE_CONFIG/ode-axis2.properties $ENGINE_PACKAGES/tmp/WEB-INF/conf/ode-axis2.properties
#cp $ENGINE_CONFIG/static-fragmento.properties $ENGINE_PACKAGES/tmp/WEB-INF/conf/static-fragmento.properties

#sed -i "s/\(ode\-axis2\.abstract\.cas\.fragmentRepository\.url\=\).*$/\1$FRAGMENT_WEB_SERVICE_REPO_URL/" $ENGINE_PACKAGES/tmp/WEB-INF/conf/ode-axis2.properties

#sed -i "s/\(ode\-axis2\.abstract\.cas\.adaptationManager\.url\=\).*$/\1$ADAPTATION_MANAGER_WEB_SERVICE_URL/" $ENGINE_PACKAGES/tmp/WEB-INF/conf/ode-axis2.properties

#sed -i "s/\(ode\-axis2\.fragmento\.url\=\).*$/\1$FRAGMENT_WEB_SERVICE_REPO_URL/" $ENGINE_PACKAGES/tmp/WEB-INF/conf/ode-axis2.properties


sed -i "s/\(ode\-axis2\.fragment\.injection\.plugins =\).*$/\1 $ADAPTATION_FRAGMENT_INJECTION_PLUGINS/" $ENGINE_PACKAGES/tmp/WEB-INF/conf/ode-axis2.properties

sed -i "s/\(ode\-axis2\.fragmento\.url\=\).*$/\1$FRAGMENT_WEB_SERVICE_REPO_URL/" $ENGINE_PACKAGES/tmp/WEB-INF/conf/ode-axis2.properties

sed -i "s/\(static\-fragmento\.repository\.url\=\).*$/\1$FRAGMENT_WEB_SERVICE_REPO_URL/" $ENGINE_PACKAGES/tmp/WEB-INF/conf/static-fragmento.properties

sed -i "s/\(adaptation\.engine\.url\=\).*$/\1$ADAPTATION_MANAGER_WEB_SERVICE_URL/" $ENGINE_PACKAGES/tmp/WEB-INF/conf/adaptation.properties

current_dir=$(pwd)

cd $ENGINE_PACKAGES/tmp

zip -r -u $ENGINE_PACKAGES/apache-ode-war/$WAR_PKG ./WEB-INF/conf/ode-axis2.properties > /dev/null 2>&1

zip -r -u $ENGINE_PACKAGES/apache-ode-war/$WAR_PKG ./WEB-INF/conf/static-fragmento.properties > /dev/null 2>&1

zip -r -u $ENGINE_PACKAGES/apache-ode-war/$WAR_PKG ./WEB-INF/conf/adaptation.properties > /dev/null 2>&1

cd $current_dir

#echo "Done configuring!!"

#echo "Installing ode engine $WAR_PKG ..."

#cp $ENGINE_PACKAGES/$WAR_PKG $TOMCAT_HOME/webapps/

rm -R $ENGINE_PACKAGES/tmp/


