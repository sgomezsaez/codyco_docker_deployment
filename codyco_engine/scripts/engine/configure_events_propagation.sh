#!/bin/bash

set -e

echo "Configuring Events Propagation with value $ODE_EVENTS_PROPAGATION ..."

WAR_PKG=$ODE_WAR_PACKAGE

echo "Extracting ODE WAR package "
mkdir -p $ENGINE_PACKAGES/tmp
cp $ENGINE_PACKAGES/apache-ode-war/$WAR_PKG $ENGINE_PACKAGES/tmp/$WAR_PKG


unzip $ENGINE_PACKAGES/tmp/$WAR_PKG -d $ENGINE_PACKAGES/tmp/ > /dev/null 2>&1

if [[ ! -f "$ENGINE_PACKAGES/tmp/WEB-INF/conf/ode-axis2.properties" ]]; then 
	echo "File ode-axis2.properties exists, replacing with template configuration..."
	cp $ENGINE_CONFIG/ode-axis2.properties $ENGINE_PACKAGES/tmp/WEB-INF/conf/ode-axis2.properties
fi

sed -i "s/\(ode\-axis2\.event\.propagation\=\).*$/\1$ODE_EVENTS_PROPAGATION/" $ENGINE_PACKAGES/tmp/WEB-INF/conf/ode-axis2.properties

sed -i "s/\(ode\-axis2\.auto\-suspend\=\).*$/\1$ODE_AUTO_SUSPEND/" $ENGINE_PACKAGES/tmp/WEB-INF/conf/ode-axis2.properties

current_dir=$(pwd)

cd $ENGINE_PACKAGES/tmp

zip -r -u $ENGINE_PACKAGES/apache-ode-war/$WAR_PKG ./WEB-INF/conf/ode-axis2.properties > /dev/null 2>&1

cd $current_dir

#echo "Done configuring!!"

#echo "Installing ode engine $WAR_PKG ..."

#cp $ENGINE_PACKAGES/$WAR_PKG $TOMCAT_HOME/webapps/

rm -R $ENGINE_PACKAGES/tmp/


