#!/bin/bash

set -e

echo "Configuring database access for engine database $ODE_DB_NAME and user $MYSQL_USER ..."

WAR_PKG=$ODE_WAR_PACKAGE

echo "Extracting Repository WAR package "
mkdir -p $ENGINE_PACKAGES/tmp
cp $ENGINE_PACKAGES/apache-ode-war/$WAR_PKG $ENGINE_PACKAGES/tmp/$WAR_PKG

echo "Configuring Engine package to connect to database $ODE_DB_NAME in $MYSQL_HOST:$MYSQL_PORT ..."

unzip $ENGINE_PACKAGES/tmp/$WAR_PKG -d $ENGINE_PACKAGES/tmp/ > /dev/null 2>&1

if [[ ! -f "$ENGINE_PACKAGES/tmp/WEB-INF/conf/ode-axis2.properties" ]]; then
	echo "File ode-axis2.properties, copying template configuration..."
        cp $ENGINE_CONFIG/ode-axis2.properties  $ENGINE_PACKAGES/tmp/WEB-INF/conf/ode-axis2.properties
fi

sed -i "s/\(ode\-axis2\.db\.int\.mcf\.databaseName=\).*$/\1$ODE_DB_NAME/" $ENGINE_PACKAGES/tmp/WEB-INF/conf/ode-axis2.properties

sed -i "s/\(ode\-axis2\.db\.int\.mcf\.userName=\).*$/\1$MYSQL_USER/" $ENGINE_PACKAGES/tmp/WEB-INF/conf/ode-axis2.properties

sed -i "s/\(ode\-axis2\.db\.int\.mcf\.password=\).*$/\1$MYSQL_PASS/" $ENGINE_PACKAGES/tmp/WEB-INF/conf/ode-axis2.properties


MYSQL_ODE_URL="$MYSQL_HOST"
sed -i "s/\(ode\-axis2\.db\.int\.mcf\.serverName=\).*$/\1$MYSQL_ODE_URL/" $ENGINE_PACKAGES/tmp/WEB-INF/conf/ode-axis2.properties

sed -i "s/\(ode\-axis2\.db\.int\.mcf\.portNumber=\).*$/\1$MYSQL_PORT/" $ENGINE_PACKAGES/tmp/WEB-INF/conf/ode-axis2.properties

current_dir=$(pwd)

cd $ENGINE_PACKAGES/tmp

zip -r -u $ENGINE_PACKAGES/apache-ode-war/$WAR_PKG ./WEB-INF/conf/ode-axis2.properties > /dev/null 2>&1


echo "Copying MySQL Drivers..."

cp $MYSQL_DRIVERS/mysql-connector-java-5.1.30-bin.jar $ENGINE_PACKAGES/tmp/WEB-INF/lib/mysql-connector-java-5.1.30-bin.jar
cp $MYSQL_DRIVERS/tranql-connector-mysql-common-1.1.jar $ENGINE_PACKAGES/tmp/WEB-INF/lib/tranql-connector-mysql-common-1.1.jar

zip -r -u $ENGINE_PACKAGES/apache-ode-war/$WAR_PKG ./WEB-INF/lib/mysql-connector-java-5.1.30-bin.jar > /dev/null 2>&1

zip -r -u $ENGINE_PACKAGES/apache-ode-war/$WAR_PKG ./WEB-INF/lib/tranql-connector-mysql-common-1.1.jar > /dev/null 2>&1

cd $current_dir

echo "Done configuring!!"

#echo "Installing ode engine $WAR_PKG ..."

#cp $ENGINE_PACKAGES/$WAR_PKG $TOMCAT_HOME/webapps/

rm -R $ENGINE_PACKAGES/tmp/
