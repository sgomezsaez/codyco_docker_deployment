#!/bin/bash

set -e

echo "Configuring JMS broker to connect to Broker ActiveMQ in $JMS_BROKER_HOST:$JMS_BROKER_PORT ..."

WAR_PKG=$ODE_WAR_PACKAGE

echo "Extracting ODE WAR package "
mkdir -p $ENGINE_PACKAGES/tmp
cp $ENGINE_PACKAGES/apache-ode-war/$WAR_PKG $ENGINE_PACKAGES/tmp/$WAR_PKG

echo "Configuring Engine package to connect to Broker ActiveMQ in $JMS_BROKER_HOST:$JMS_BROKER_PORT ..."

unzip $ENGINE_PACKAGES/tmp/$WAR_PKG -d $ENGINE_PACKAGES/tmp/ > /dev/null 2>&1

if [ -f "$ENGINE_PACKAGES/tmp/WEB-INF/conf/ode-axis2.properties" ]; then 
	echo "File ode-axis2.properties exists, replacing with template configuration..."
	cp $ENGINE_CONFIG/ode-axis2.properties $ENGINE_PACKAGES/tmp/WEB-INF/conf/ode-axis2.properties
fi

#JMS_BROKER_URL="failover\:\(tcp\:\/\/$JMS_BROKER_HOST\:$JMS_BROKER_PORT\)"
JMS_BROKER_URL="tcp\:\/\/$JMS_BROKER_HOST\:$JMS_BROKER_PORT"
## Active MQ Local Configuration
#JMS_BROKER_URL="tcp\:\/\/localhost\:$JMS_BROKER_PORT"
sed -i "s/\(ode\-axis2\.activemq\.url\=\).*$/\1$JMS_BROKER_URL/" $ENGINE_PACKAGES/tmp/WEB-INF/conf/ode-axis2.properties

current_dir=$(pwd)

cd $ENGINE_PACKAGES/tmp

zip -r -u $ENGINE_PACKAGES/apache-ode-war/$WAR_PKG ./WEB-INF/conf/ode-axis2.properties > /dev/null 2>&1

cd $current_dir

#echo "Done configuring!!"

#echo "Installing ode engine $WAR_PKG ..."

#cp $ENGINE_PACKAGES/$WAR_PKG $TOMCAT_HOME/webapps/

rm -R $ENGINE_PACKAGES/tmp/


