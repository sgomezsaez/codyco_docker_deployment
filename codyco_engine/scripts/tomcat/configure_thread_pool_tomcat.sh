#!/bin/bash

set -e

echo "Configuring tomcat connector with maxThreads=$TOMCAT_MAX_THREADS, minSpareThreads=$TOMCAT_MIN_SPARE_THREADS and connectionTimeout=$TOMCAT_CONNECTION_TIMEOUT  ..."

cp configuration/tomcat/server.xml $TOMCAT_HOME/conf/server.xml


sed -i "s/maxThreads\=\"\$TOMCAT_MAX_THREADS\"/maxThreads\=\"$TOMCAT_MAX_THREADS\"/" $TOMCAT_HOME/conf/server.xml

sed -i "s/minSpareThreads\=\"\$TOMCAT_MIN_SPARE_THREADS\"/minSpareThreads\=\"$TOMCAT_MIN_SPARE_THREADS\"/" $TOMCAT_HOME/conf/server.xml

sed -i "s/connectionTimeout\=\"\$TOMCAT_CONNECTION_TIMEOUT\"/connectionTimeout\=\"$TOMCAT_CONNECTION_TIMEOUT\"/" $TOMCAT_HOME/conf/server.xml

echo "Done Configuring..."
