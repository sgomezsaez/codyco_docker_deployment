#!/bin/bash

#apt-get install -y tomcat7
#ln -s /usr/share/tomcat7/lib /var/lib/tomcat7/lib
#ln -s /var/lib/tomcat7 /srv/tomcat7

# Install tomcat (inspired by jolokia/tomcat-7.0)
wget http://archive.apache.org/dist/tomcat/tomcat-7/v${TOMCAT_VERSION}/bin/apache-tomcat-${TOMCAT_VERSION}.tar.gz -O /tmp/catalina.tar.gz
tar -zxf /tmp/catalina.tar.gz -C /opt
ln -s /opt/apache-tomcat-${TOMCAT_VERSION} /opt/tomcat
rm /tmp/catalina.tar.gz

# Remove unneeded stuff
 rm -rf /opt/tomcat/webapps/examples
 rm -rf /opt/tomcat/webapps/docs

# Replace 'random' with 'urandom' for quicker startups
rm /dev/random
ln -s /dev/urandom /dev/random

# Links for legacy stuff
ln -s /opt/tomcat /var/lib/tomcat7
ln -s /opt/tomcat /srv/tomcat7
