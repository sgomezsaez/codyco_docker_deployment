#!/bin/bash

set -e 

echo "Configuring tomcat supervisor file in $SUPERVISOR_CONFIG ..."

#sed -i "s/.*#environment_tomcat*/environment=JAVA_OPTS=\"\-XX:MaxPermSize=2058m \-Dport.http.nonssl=\$CATALINA_BASE_PORT -Dport.shutdown=\$CATALINA_SHUTDOWN_PORT \-Dport.ajp.port=\$CATALINA_AJP_PORT\"/" $SUPERVISOR_CONFIG/supervisord.tomcat.conf

JAVA_OPTS_TRACE="$JAVA_OPTS -Dcom.sun.xml.ws.transport.http.client.HttpTransportPipe.dump=true"

sed -i "s/.*#environment_tomcat*/environment=JAVA_OPTS=\"$JAVA_OPTS_TRACE\"/" $SUPERVISOR_CONFIG/supervisord.tomcat.conf

echo "Done Configuring tomcat supervisor file in $SUPERVISOR_CONFIG"



