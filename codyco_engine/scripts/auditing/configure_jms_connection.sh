#!/bin/bash

set -e

echo "Configuring active mq connection for the auditing service..."

WAR_PKG="SimTechAuditing-1.0-SNAPSHOT.war"
echo "Extracting Repository WAR package "
mkdir -p $AUDITING_PACKAGES/tmp
cp $AUDITING_PACKAGES/$WAR_PKG $AUDITING_PACKAGES/tmp/$WAR_PKG

echo "Configuring Auditing file to connect to JMS Broker in host $JMS_BROKER_HOST:$JMS_BROKER_PORT ..."

unzip $AUDITING_PACKAGES/tmp/$WAR_PKG -d $AUDITING_PACKAGES/tmp/ > /dev/null 2>&1

#JMS_BROKER_URL="failover\:\(tcp\:\/\/$JMS_BROKER_HOST\:$JMS_BROKER_PORT\)\?wireFormat\.maxInactivityDuration\=0"
#JMS_BROKER_URL="failover\:\(tcp\:\/\/$JMS_BROKER_HOST\:$JMS_BROKER_PORT\)"
JMS_BROKER_URL="tcp\:\/\/$JMS_BROKER_HOST\:$JMS_BROKER_PORT"
## Active MQ Local Configuration
#JMS_BROKER_URL="tcp:\/\/localhost:$JMS_BROKER_PORT"
sed -i "s/<property name=\"brokerURL\" value=\"[^\"]*/<property name=\"brokerURL\" value=\"$JMS_BROKER_URL/g" $AUDITING_PACKAGES/tmp/WEB-INF/spring/messaging/spring-jms.xml

current_dir=$(pwd)

cd $AUDITING_PACKAGES/tmp

zip -r -u $AUDITING_PACKAGES/$WAR_PKG ./WEB-INF/spring/messaging/spring-jms.xml

cd $current_dir

echo "Done configuring!!"

#echo "Installing Auditing Service..."

#cp $AUDITING_PACKAGES/$WAR_PKG $TOMCAT_HOME/webapps

#echo "Auditing Service ($WAR_PKG) installed in $TOMCAT_HOME/webapps !!"

rm -R $AUDITING_PACKAGES/tmp/
