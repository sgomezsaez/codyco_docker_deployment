#!/bin/bash

set -e

echo "Configuring database access for auditing database $AUDIT_DB_NAME and user $MYSQL_USER ..."

WAR_PKG="SimTechAuditing-1.0-SNAPSHOT.war"
echo "Extracting Repository WAR package "
mkdir -p $AUDITING_PACKAGES/tmp
cp $AUDITING_PACKAGES/$WAR_PKG $AUDITING_PACKAGES/tmp/$WAR_PKG

echo "Configuring Auditing file to connect to database $AUDIT_DB_NAME in $MYSQL_HOST ..."

unzip $AUDITING_PACKAGES/tmp/$WAR_PKG -d $AUDITING_PACKAGES/tmp/ > /dev/null 2>&1

sed -i "s/<property name=\"username\" value=\"[^\"]*/<property name=\"username\" value=\"$MYSQL_USER/g" $AUDITING_PACKAGES/tmp/WEB-INF/spring/database/spring-datasource.xml

sed -i "s/<property name=\"password\" value=\"[^\"]*/<property name=\"password\" value=\"$MYSQL_PASS/g" $AUDITING_PACKAGES/tmp/WEB-INF/spring/database/spring-datasource.xml

MYSQL_REPO_URL="jdbc\:mysql\:\/\/$MYSQL_HOST\:$MYSQL_PORT\/$AUDIT_DB_NAME"
sed -i "s/<property name=\"url\" value=\"[^\"]*/<property name=\"url\" value=\"$MYSQL_REPO_URL/g" $AUDITING_PACKAGES/tmp/WEB-INF/spring/database/spring-datasource.xml

sed -i "s/<property name=\"schema\" value=\"[^\"]*/<property name=\"schema\" value=\"$AUDIT_DB_NAME/g" $AUDITING_PACKAGES/tmp/WEB-INF/spring/database/spring-datasource.xml

current_dir=$(pwd)

cd $AUDITING_PACKAGES/tmp

zip -r -u $AUDITING_PACKAGES/$WAR_PKG ./WEB-INF/spring/database/spring-datasource.xml  > /dev/null 2>&1

cd $current_dir

echo "Done configuring!!"

#echo "Installing Auditing Service..."

#cp $AUDITING_PACKAGES/$WAR_PKG $TOMCAT_HOME/webapps

#echo "Auditing Service ($WAR_PKG) installed in $TOMCAT_HOME/webapps !!"

rm -R $AUDITING_PACKAGES/tmp/
