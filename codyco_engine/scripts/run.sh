#!/bin/bash

set -e

odewar=$ODE_WAR_PACKAGE

echo "Configuring Tomcat access credentials..."
$TOMCAT_SCRIPTS/configure_access.sh

echo "Configuring Thread Pool Connection..."
$TOMCAT_SCRIPTS/configure_thread_pool_tomcat.sh

#echo "Configuring Tomcat on port $TOMCAT_PORT ..."
#$TOMCAT_SCRIPTS/configure_port.sh

echo "Configuring Audit database access for database $AUDIT_DB_NAME ..."
$AUDITING_SCRIPTS/configure_database_connection.sh

echo "Configuring JMS access for Audit Service for broker $JMS_BROKER_HOST:$JMS_BROKER_PORT ..."
$AUDITING_SCRIPTS/configure_jms_connection.sh

echo "Installing Auditing Service..."
cp $AUDITING_PACKAGES/SimTechAuditing-1.0-SNAPSHOT.war $TOMCAT_HOME/webapps/SimTechAuditing-1.0-SNAPSHOT.war

echo "Auditing Service installed in $TOMCAT_HOME/webapps !!"

if [[ ! -f $ENGINE_PACKAGES/apache-ode-war/$odewar ]]; then
	echo "Engine war package not found in $ENGINE_PACKAGES/apache-ode-war/ , copying package..."
        cp $ENGINE_PACKAGES/$odewar $ENGINE_PACKAGES/apache-ode-war/$odewar
fi

echo "Configuring ODE JMS Connection on host $JMS_BROKER_HOST:$JMS_BROKER_PORT ..."
$ENGINE_SCRIPTS/configure_jms_connection.sh

echo "Configuring Events Propagation with value $ODE_EVENTS_PROPAGATION ..."
$ENGINE_SCRIPTS/configure_events_propagation.sh

echo "Configuring ODE Database $ODE_DB_NAME Connection on host $MYSQL_HOST:$MYSQL_PORT ..."
$ENGINE_SCRIPTS/configure_database_connection.sh

echo "Configuring Abstract CAS Connection on hosts for Fragment Repository and Adaptation Manager ..."
$ENGINE_SCRIPTS/configure_abstract_cas_refs.sh

echo "Installing Engine..."
cp $ENGINE_PACKAGES/apache-ode-war/$odewar $TOMCAT_HOME/webapps/$odewar

echo "Engine ODE installed in $TOMCAT_HOME/webapps !!"

echo "Configuring env variables in supervisord tomcat config file..."
$TOMCAT_SCRIPTS/configure_supervisor.sh

sleep 3

exec supervisord -n -c /supervisor/supervisord.conf
