#!/bin/bash
set -e

#sudo docker build -t codyco/mysql .

if [ -d "/etc/mysql" ]; then
	echo "creating mysql folder"
        sudo rm -R /etc/mysql
fi
sudo mkdir -p /etc/mysql

if [ ! -d "/etc/mysql/db" ]; then
	echo "creating db folder"
        sudo mkdir -p /etc/mysql/db
fi

#sudo docker build -t codyco/mysql .

IPADD=$(ifconfig eth0 | grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}')

#sudo docker run -d -p $IPADD:3306:3306 -net host -v /etc/mysql/db:/var/lib/mysql -t codyco/mysql

#sudo docker run -d -v /etc/mysql/db/:/var/lib/mysql codyco/mysql /bin/bash -c "/usr/bin/mysql_install_db"

sudo docker run -d -p $IPADD:$MYSQL_PORT:$MYSQL_PORT -t -net host -v /etc/mysql/db:/var/lib/mysql codyco/mysql /scripts/mysql/run.sh
