#!/bin/bash

set -e

/usr/bin/mysqld_safe > /dev/null 2>&1 &

RET=1
while [[ RET -ne 0 ]]; do
    echo "=> Waiting for confirmation of MySQL service startup"
    sleep 5
    mysql -uroot -e "status" > /dev/null 2>&1
    RET=$?
done

echo "=> Verifying if database $ODE_DB_NAME exists..."

output=$(mysql -uroot -e "SELECT schema_name FROM information_schema.schemata WHERE schema_name = 'ode'" information_schema)
if [[ ! -z "${output}" ]]; then
  mysql -uroot -e "DROP DATABASE ode"
fi

echo "=> Creating database $ODE_DB_NAME"

mysql -uroot -e "CREATE DATABASE IF NOT EXISTS $ODE_DB_NAME"

echo "Database $ODE_DB_NAME created"

mysql -uroot $ODE_DB_NAME < $ODE_MYSQL_SCRIPTS/mysql.sql
mysql -uroot $ODE_DB_NAME < $ODE_MYSQL_SCRIPTS/createUser.sql

#mysql -uroot -e "CREATE USER '${MYSQL_USER}'@'%' IDENTIFIED BY '$MYSQL_PASS'"
#mysql -uroot -e "GRANT ALL PRIVILEGES ON *.* TO '${MYSQL_USER}'@'%' WITH GRANT OPTION"
#mysql -uroot -e "GRANT ALL PRIVILEGES ON *.* TO '${MYSQL_USER}'@'129.69.%' IDENTIFIED BY PASSWORD '$MYSQL_PASS' WITH GRANT OPTION"
#mysql -uroot -e "GRANT ALL PRIVILEGES ON *.* TO '${MYSQL_USER}'@'%.iaas.uni-stuttgart.de' IDENTIFIED BY PASSWORD '$MYSQL_PASS' WITH GRANT OPTION"

#mysqladmin -u $MYSQL_USER password "$MYSQL_PASS"
#/etc/init.d/mysql restart

echo "=> Done!"

echo "========================================================================"
echo "You can now connect to this MySQL Server using:"
echo ""
echo "    mysql -u$MYSQL_USER -p$MYSQL_PASS -h<host> -P<port>"
echo ""
echo "Please remember to change the above password as soon as possible!"
echo "MySQL user 'root' has no password but only allows local connections"
echo "========================================================================"

mysqladmin -uroot shutdown

