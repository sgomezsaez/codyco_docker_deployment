#!/bin/bash

set -e

echo "=> An empty or uninitialized MySQL volume is detected in $VOLUME_HOME"
echo "=> Installing MySQL database"
if [ ! -f /usr/share/mysql/my-default.cnf ]; then
        cp /etc/mysql/conf.d/my.cnf /usr/share/mysql/my-default.cnf
fi

#cp /config/my.cnf /var/lib/mysql/my.cnf
#cp /config/mysqld_charset.cnf /var/lib/mysql/mysqld_charset.cnf


mysql_install_db > /dev/null 2>&1
echo "=> Done installing database in $VOLUME_HOME!"

echo "Creating databases $ODE_DB_NAME , and user $MYSQL_USER..."
/scripts/mysql/mysql_ode.sh
echo "=>Done creating databases $ODE_DB_NAME , and user $MYSQL_USER!"

echo "Creating databases $AUDIT_DB_NAME , and user $MYSQL_USER..."
/scripts/mysql/mysql_auditing.sh
echo "=>Done creating databases $AUDIT_DB_NAME and user $MYSQL_USER!"

#exec mysqld_safe
exec supervisord -n -c /supervisor/supervisord.conf
