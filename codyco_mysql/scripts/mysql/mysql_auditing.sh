#!/bin/bash

set -e

/usr/bin/mysqld_safe > /dev/null 2>&1 &

RET=1
while [[ RET -ne 0 ]]; do
    echo "=> Waiting for confirmation of MySQL service startup"
    sleep 5
    mysql -uroot -e "status" > /dev/null 2>&1
    RET=$?
done

echo "=> Creating database $AUDIT_DB_NAME"


mysql -uroot -e "CREATE DATABASE $AUDIT_DB_NAME"

#PASS=${MYSQL_PASS:-$(pwgen -s 12 1)}

mysql -uroot -e "GRANT ALL PRIVILEGES ON auditing.* TO '${MYSQL_USER}'@'%' WITH GRANT OPTION"
#mysql -uroot -e "GRANT ALL PRIVILEGES ON *.* TO '${MYSQL_USER}'@'129.69.%' IDENTIFIED BY PASSWORD '$MYSQL_PASS' WITH GRANT OPTION"
#mysql -uroot -e "GRANT ALL PRIVILEGES ON *.* TO '${MYSQL_USER}'@'%.iaas.uni-stuttgart.de' IDENTIFIED BY PASSWORD '$MYSQL_PASS' WITH GRANT OPTION"

#mysqladmin -u $MYSQL_USER password "$MYSQL_PASS"
#/etc/init.d/mysql restart

echo "=> Done!"

echo "========================================================================"
echo "You can now connect to this MySQL Server using:"
echo ""
echo "    mysql -u$MYSQL_USER -p$MYSQL_PASS -h<host> -P<port>"
echo ""
echo "Please remember to change the above password as soon as possible!"
echo "MySQL user 'root' has no password but only allows local connections"
echo "========================================================================"

mysqladmin -uroot shutdown
