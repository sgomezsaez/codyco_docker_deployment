#!/bin/bash
export MYSQL_SCRIPTS=/scripts/mysql 
export ODE_MYSQL_SCRIPTS=/scripts/mysql/ode
export VOLUME_HOME=/var/lib/mysql
export MYSQL_USER=test
export MYSQL_PASS=test
export ODE_DB_NAME=ode
export AUDIT_DB_NAME=auditing
