#!/bin/bash

set -e

POSTGRES_LOG=/etc/postgres/log
POSTGRES_SUPERVISOR_LOG=/etc/postgres/supervisor/log
POSTGRES_MAIN_DIR=/etc/postgres/main
POSTGRES_LIB_MAIN=/etc/postgres/var/lib/main
POSTGRES_DATA_DIR=/etc/postgres/data

echo "Creating postgres folders locally"

if [ -d "/etc/postgres" ]; then
	echo "deleting database stored in host..."
	sudo rm -R /etc/postgres
fi

sudo mkdir -p /etc/postgres/log
sudo mkdir -p /etc/postgres/supervisor/log
sudo mkdir -p /etc/postgres/main
sudo mkdir -p /etc/postgres/var/lib/main
sudo mkdir -p /etc/postgres/data

#sudo docker build -t codyco/postgres .

IPADD=$(ifconfig eth0 | grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}')

#sudo docker run -d -p $IPADD:3306:3306 -net host -v /etc/mysql/db:/var/lib/mysql -t codyco/mysql

#sudo docker run -d -v /etc/mysql/db/:/var/lib/mysql codyco/mysql /bin/bash -c "/usr/bin/mysql_install_db"
sudo docker run -d -p $IPADD:$FRAGMENTO_REPO_PORT:$FRAGMENTO_REPO_PORT -t -net host -v $POSTGRES_LOG:/var/log/postgresql -v $POSTGRES_SUPERVISOR_LOG:/var/log/supervisor -v $POSTGRES_MAIN_DIR:/etc/postgresql/9.3/main -v $POSTGRES_LIB_MAIN:/var/lib/postgresql/9.3/main -v $POSTGRES_DATA_DIR:/data/postgres codyco/postgres
