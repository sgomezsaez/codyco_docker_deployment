#!/bin/bash
export POSTGRES_LOG=/var/log/postgresql
export POSTGRES_SUPERVISOR_LOG=/var/log/supervisor
export POSTGRES_MAIN_DIR=/etc/postgresql/9.3/main
export POSTGRES_LIB_MAIN=/var/lib/postgresql/9.3/main
export POSTGRES_VERSION=9.3
export POSTGRES_SCRIPTS=/scripts/postgres
export POSTGRES_USER=postgres 
export POSTGRES_PASSWORD=postgres
export FRAGMENTO_DB_NAME=repository
export POSTGRES_BIN=/usr/lib/postgresql/9.3/bin
export POSTGRES_DATA_DIR=/data/postgres
