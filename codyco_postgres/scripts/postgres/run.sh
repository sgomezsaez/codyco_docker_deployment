#!/bin/bash

#apt-get install -y postgresql pgadmin3

#touch $POSTGRES_LOG
#chown postgres $POSTGRES_LOG
#set -e

echo "Creating database $FRAGMENTO_DB_NAME"

chown postgres $POSTGRES_DATA_DIR
chmod 777 $POSTGRES_DATA_DIR
chown postgres $POSTGRES_LOG
chmod 777 $POSTGRES_LOG

su -c "$POSTGRES_BIN/initdb $POSTGRES_DATA_DIR" postgres

$POSTGRES_SCRIPTS/start.sh

sleep 5

su -c "$POSTGRES_BIN/createdb $FRAGMENTO_DB_NAME" postgres
su -c "$POSTGRES_BIN/psql -U postgres -d postgres -c \"alter user $POSTGRES_USER with password '$POSTGRES_PASSWORD'\"" postgres

$POSTGRES_SCRIPTS/stop.sh

sleep 5

echo "host all  all    0.0.0.0/0  md5" >> $POSTGRES_DATA_DIR/pg_hba.conf
#echo "listen_addresses='*'" >> $POSTGRES_MAIN_DIR/postgresql.conf

supervisord -n -c /supervisor/supervisord.conf
