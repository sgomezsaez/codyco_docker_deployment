#!/bin/bash

set -e

# Setting Sys Environment Variables
PROVISIONING_HOME_DIR=$(pwd)
IPADD=$(ifconfig eth0 | grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}')

## Codyco Base
export CODYCO_BASE_CONFIG_DIR=$PROVISIONING_HOME_DIR/codyco_base/config/supervisor

## ActiveMQ
export ACTIVEMQ_VERSION=5.4.3
export ACTIVEMQ_JAVA_OPTS="-XX:MaxPermSize=2058m" 
export ACTIVEMQ_DOWNLOAD_URL="http://archive.apache.org/dist/activemq/apache-activemq/5.4.3/apache-activemq-5.4.3-bin.tar.gz"
export JMS_BROKER_PORT="61616"
export ACTIVEMQ_ADMIN_PORT="8161"
export ACTIVEMQ_JMS_BROKER_PORT=$JMS_BROKER_PORT
export ACTIVEMQ_MANAGEMENT_PORT1="61612"
export ACTIVEMQ_MANAGEMENT_PORT2="61613"

export ACTIVEMQ_URL="tcp\:\/\/$IPADD\:$JMS_BROKER_PORT"

## MySQL DB (Codyco Engine and Auditing System DB)
export MYSQL_USER="test"
export MYSQL_PASS="test"
export ODE_DB_NAME="ode"
export AUDIT_DB_NAME="auditing"
export MYSQL_PORT="3306"

## PostgreSQL DB (Fragment Repository)
export POSTGRES_USER="postgres"
export POSTGRES_PASSWORD="postgres"
export FRAGMENTO_DB_NAME="repository"

## Tomcat Config
export TOMCAT_DOWNLOAD_URL="http://archive.apache.org/dist/tomcat/tomcat-7/v7.0.54/bin/apache-tomcat-7.0.54.tar.gz"
export TOMCAT_USER="allow" 
export TOMCAT_PASSWORD="allow"
export TOMCAT_DEFAULT_PORT="8080"
export TOMCAT_DEFAULT_SHUTDOWN_PORT="8005"
export TOMCAT_DEFAULT_AJP_PORT="8009"

## Fragmento App
export AXIS2_DOWNLOAD_URL="http://archive.apache.org/dist/axis/axis2/java/core/1.5.6/axis2-1.5.6-war.zip"
export FRAGMENTO_DB_NAME="repository"
export FRAGMENTO_REPO_PORT=5432
export FRAGMENTO_WAR_PACKAGE_NAME="Repository.war"
export TOMCAT_FRAGMENTO_START_PORT=8050
export TOMCAT_FRAGMENTO_SHUTDOWN_PORT=8180
export TOMCAT_FRAGMENTO_AJP_PORT=8210

## Codyco Engine
export ENGINE_PACKAGE_NAME="ode.war"
export ODE_NAME="ode"
export TOMCAT_ENGINE_START_PORT=8080
export TOMCAT_ENGINE_SHUTDOWN_PORT=8005
export TOMCAT_ENGINE_AJP_PORT=8009 

export ODE_JAVA_OPTS="-Xms4096m -Xmx4096m"
export CATALINA_OPTS="-Xms4096m -Xmx4096m -XX:MaxNewSize=1024m -XX:MaxPermSize=512m"

export ODE_PROCESS_SHARED_HOST="$(pwd)/processes"
export ODE_PROCESS_SHARED="processes"

export FRAGMENT_WEB_SERVICE_REPO_URL="http\:\/\/$IPADD\:$TOMCAT_FRAGMENTO_START_PORT\/Repository"

## Codico Engine - Adaptation Framework Configuration
export ADAPTATION_FRAGMENT_INJECTION_PLUGINS="de\.unistuttgart\.iaas\.fragment\.staticInjection\.StaticFragmentoRetrievalService\,de\.unistuttgart\.iaas\.ode\.AdaptationRetrievalService"
export ADAPTATION_MANAGER_WEB_SERVICE_URL="http\:\/\/129\.69\.214\.128\:8080\/AdaptationEngineAxis2\/services\/AdaptationEngineService\?wsdl"

## Codyco Engine - Events Propagation
export ODE_EVENTS_PROPAGATION="true"
export ODE_AUTO_SUSPEND="false"

## Codyco Engine - Tomcat Tuning
export TOMCAT_MAX_THREADS=1000
export TOMCAT_MIN_SPARE_THREADS=200
export TOMCAT_CONNECTION_TIMEOUT=60000

echo "#### Docker Installation ####"

if docker -v >/dev/null 2>&1; then
	echo "Docker $(docker -v) already installed"
else
	echo "Installing Docker..."
	chmod +x install_docker.sh
	sudo ./install_docker.sh

	echo "Docker $(docker -v) installed... provisioning containers..."
fi

echo "#### Building Docker containers... #### "
echo "--> codyco/base"
cd codyco_base/
mv Dockerfile Dockerfile.tmp
cat Dockerfile.tmp | envsubst '$CODYCO_BASE_CONFIG_DIR' > Dockerfile
sudo docker build -t codyco/base .
mv Dockerfile.tmp Dockerfile
echo "--> codyco/base built!!"

cd ../codyco_activemq/
echo "--> codyco/activemq"
mv Dockerfile Dockerfile.tmp
cat Dockerfile.tmp | envsubst '$ACTIVEMQ_VERSION $ACTIVEMQ_JAVA_OPTS $ACTIVEMQ_DOWNLOAD_URL' > Dockerfile
sudo docker build -t codyco/activemq .
mv Dockerfile.tmp Dockerfile
echo "--> codyco/activemq built!!"

cd ../codyco_mysql/
echo "--> codyco/mysql"
mv Dockerfile Dockerfile.tmp
cat Dockerfile.tmp | envsubst '$MYSQL_USER $MYSQL_PASS $ODE_DB_NAME $AUDIT_DB_NAME' > Dockerfile
sudo docker build -t codyco/mysql .
mv Dockerfile.tmp Dockerfile
echo "--> codyco/mysql built!!"

cd ../codyco_postgres/
echo "--> codyco/postgres"
mv Dockerfile Dockerfile.tmp
cat Dockerfile.tmp | envsubst '$POSTGRES_USER $POSTGRES_PASSWORD $FRAGMENTO_DB_NAME' > Dockerfile
sudo docker build -t codyco/postgres .
mv Dockerfile.tmp Dockerfile
echo "--> codyco/postgres built!!"

cd ../codyco_fragmento/
echo "--> codyco/fragmento"
mv Dockerfile Dockerfile.tmp
cat Dockerfile.tmp | envsubst '$TOMCAT_DOWNLOAD_URL $TOMCAT_USER $TOMCAT_PASSWORD $FRAGMENTO_DB_NAME $TOMCAT_FRAGMENTO_START_PORT $TOMCAT_FRAGMENTO_SHUTDOWN_PORT $TOMCAT_FRAGMENTO_AJP_PORT $AXIS2_DOWNLOAD_URL' > Dockerfile
sudo docker build -t codyco/fragmento .
mv Dockerfile.tmp Dockerfile
echo "--> codyco/fragmento built!!"

echo "Building Processes Shared Folder..."
mkdir -p $ODE_PROCESS_SHARED_HOST
cd ../codyco_engine/
echo "--> codyco/engine"
mv Dockerfile Dockerfile.tmp
cat Dockerfile.tmp | envsubst '$TOMCAT_ENGINE_START_PORT $TOMCAT_ENGINE_SHUTDOWN_PORT $TOMCAT_ENGINE_AJP_PORT $MYSQL_USER $MYSQL_PASS $TOMCAT_USER $TOMCAT_PASSWORD $ODE_NAME $AUDIT_DB_NAME $TOMCAT_DOWNLOAD_URL $AXIS2_DOWNLOAD_URL $ENGINE_PACKAGE_NAME $FRAGMENT_WEB_SERVICE_REPO_URL $ADAPTATION_MANAGER_WEB_SERVICE_URL $ADAPTATION_FRAGMENT_INJECTION_PLUGINS $ODE_PROCESS_SHARED $ODE_EVENTS_PROPAGATION $ODE_JAVA_OPTS $CATALINA_OPTS $TOMCAT_MAX_THREADS $TOMCAT_MIN_SPARE_THREADS $TOMCAT_CONNECTION_TIMEOUT $ACTIVEMQ_URL $ODE_AUTO_SUSPEND' > Dockerfile
sudo docker build -t codyco/engine .
mv Dockerfile.tmp Dockerfile
echo "--> codyco/engine built!!"
cd ..

sleep 5

echo "Now Provisioning Docker containers..."
echo "LogFile $('date')" >> provision_containers.log

echo "--> codyco/activemq..."
cd codyco_activemq/
chmod +x run_container.sh
nohup ./run_container.sh >>../provision_containers.log 2>&1 & 
cd ..
echo "--> codyco/activemq running!!"

sleep 5

echo "--> codyco/mysql"
cd codyco_mysql/
chmod +x run_container.sh
nohup ./run_container.sh >>../provision_containers.log 2>&1 &
cd ..
echo "--> codyco/mysql running!!"

sleep 10

echo "--> codyco/postgres"
cd codyco_postgres/
chmod +x run_container.sh
nohup ./run_container.sh >>../provision_containers.log 2>&1 &
cd ..
echo "--> codyco/postgres running!!"

sleep 10

echo "--> codyco/fragmento"
cd codyco_fragmento/
chmod +x run_container.sh
nohup ./run_container.sh >>../provision_containers.log 2>&1 &
cd ..
echo "--> codyco/fragmento running!!"

sleep 10

echo "--> codyco/engine"
cd codyco_engine/
chmod +x run_container.sh
nohup ./run_container.sh >>../provision_containers.log 2>&1 &
cd ..
echo "--> codyco/engine running!!"

sleep 15

echo "#### CoDyCo Execution Environment provisioned ####"

echo "Minor adjustment for the auditing table"
sleep 5
echo "Ranaming Table InstanceIndex to instanceIndex in auditing database..."

while [ $(mysql -N -s -u$MYSQL_USER -p$MYSQL_PASS -h $IPADD -e "select count(*) from information_schema.tables where table_schema='$AUDIT_DB_NAME' and table_name='InstanceIndex';") -eq 0 ]; do
	echo "Table not yet Created in $AUDIT_DB_NAME, waiting..."
	sleep 5
done

echo "Table InstanceIndex created in $AUDIT_DB_NAME. Renaming..."
mysql -u$MYSQL_USER -p$MYSQL_PASS -h $IPADD $AUDIT_DB_NAME -e "alter table InstanceIndex rename to instanceIndex;"

# ODE Schema modifications
while [ $(mysql -N -s -u$MYSQL_USER -p$MYSQL_PASS -h $IPADD -e "select count(*) from information_schema.tables where table_schema='$ODE_DB_NAME' and table_name='ODE_XML_DATA';") -eq 0 ]; do
        echo "Table ODE_XML_DATA not yet Created in $ODE_DB_NAME, waiting..."
        sleep 5
done

echo "Table ODE_XML_DATA created in $ODE_DB_NAME. Modifying data type for DATA..."
mysql -u$MYSQL_USER -p$MYSQL_PASS -h $IPADD $ODE_DB_NAME -e " ALTER TABLE ODE_XML_DATA MODIFY DATA LONGTEXT;"


while [ $(mysql -N -s -u$MYSQL_USER -p$MYSQL_PASS -h $IPADD -e "select count(*) from information_schema.tables where table_schema='$ODE_DB_NAME' and table_name='ODE_MESSAGE';") -eq 0 ]; do
        echo "Table ODE_MESSAGE not yet Created in $ODE_DB_NAME, waiting..."
        sleep 5
done

echo "Table ODE_MESSAGE created in $ODE_DB_NAME. Modifying data type for DATA..."
mysql -u$MYSQL_USER -p$MYSQL_PASS -h $IPADD $ODE_DB_NAME -e " ALTER TABLE ODE_MESSAGE MODIFY DATA LONGTEXT;"

mysql -u$MYSQL_USER -p$MYSQL_PASS -h $IPADD $ODE_DB_NAME -e " ALTER TABLE ODE_MESSAGE MODIFY HEADER LONGTEXT;"

while [ $(mysql -N -s -u$MYSQL_USER -p$MYSQL_PASS -h $IPADD -e "select count(*) from information_schema.tables where table_schema='$ODE_DB_NAME' and table_name='ODE_SNAPSHOT_VARIABLE';") -eq 0 ]; do
        echo "Table ODE_SNAPSHOT_VARIABLE not yet Created in $ODE_DB_NAME, waiting..."
        sleep 5
done

echo "Table ODE_SNAPSHOT_VARIABLE created in $ODE_DB_NAME. Modifying data type for DATA..."
mysql -u$MYSQL_USER -p$MYSQL_PASS -h $IPADD $ODE_DB_NAME -e " ALTER TABLE ODE_SNAPSHOT_VARIABLE MODIFY DATA LONGTEXT;"

#Monitoring
#sudo docker run --volume=/:/rootfs:ro --volume=/var/run:/var/run:rw --volume=/sys:/sys:ro --volume=/var/lib/docker/:/var/lib/docker:ro --publish=$(ifconfig eth0 | grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}'):10000:8080 --detach=true --name=cadvisor google/cadvisor:latest

#docker run -d --name scout-agent -v /proc:/host/proc:ro -v /etc/mtab:/host/etc/mtab:ro -v /var/run/docker.sock:/host/var/run/docker.sock:ro -v `pwd`/scoutd.yml:/etc/scout/scoutd.yml --restart=always --net=host --privileged scoutapp/docker-scout

