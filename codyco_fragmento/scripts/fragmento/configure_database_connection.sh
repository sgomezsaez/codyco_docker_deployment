#!/bin/bash

#apt-get install -y tomcat7
#ln -s /usr/share/tomcat7/lib /var/lib/tomcat7/lib
#ln -s /var/lib/tomcat7 /srv/tomcat7

# Install tomcat (inspired by jolokia/tomcat-7.0)
#wget http://archive.apache.org/dist/tomcat/tomcat-7/v${TOMCAT_VERSION}/bin/apache-tomcat-${TOMCAT_VERSION}.tar.gz -O /tmp/catalina.tar.gz
#tar -zxf /tmp/catalina.tar.gz -C /opt
#ln -s /opt/apache-tomcat-${TOMCAT_VERSION} /opt/tomcat
#rm /tmp/catalina.tar.gz

set -e

echo "Configuring database access for fragmento database $FRAGMENTO_DB_NAME and user $TOMCAT_USER ..."

WAR_PKG="$FRAGMENTO_WAR_PACKAGE_NAME"
echo "Extracting Repository WAR package "
mkdir -p $FRAGMENTO_PACKAGES/tmp
cp $FRAGMENTO_PACKAGES/$WAR_PKG $FRAGMENTO_PACKAGES/tmp/$WAR_PKG

echo "Configuring Fragmento file to connect to database $FRAGMENTO_DB_NAME in $FRAGMENTO_REPO_HOST ..."

unzip $FRAGMENTO_PACKAGES/tmp/$WAR_PKG -d $FRAGMENTO_PACKAGES/tmp/

sed -i "s/username=\"[^\"]*/username=\"$POSTGRES_USER/g" $FRAGMENTO_PACKAGES/tmp/META-INF/context.xml

sed -i "s/password=\"[^\"]*/password=\"$POSTGRES_PASSWORD/g" $FRAGMENTO_PACKAGES/tmp/META-INF/context.xml

FRAGMENTO_REPO_URL="jdbc\:postgresql\:\/\/$FRAGMENTO_REPO_HOST\:$FRAGMENTO_REPO_PORT\/$FRAGMENTO_DB_NAME"
sed -i "s/url=\"[^\"]*/url=\"$FRAGMENTO_REPO_URL/g" $FRAGMENTO_PACKAGES/tmp/META-INF/context.xml

current_dir=$(pwd)

cd $FRAGMENTO_PACKAGES/tmp

zip -r -u $FRAGMENTO_PACKAGES/$WAR_PKG ./META-INF/context.xml

cd $current_dir

echo "Done configuring!!"

echo "Installing Fragmento..."

cp $FRAGMENTO_PACKAGES/$WAR_PKG $TOMCAT_HOME/webapps

echo "Fragmento ($WAR_PKG) installed in $TOMCAT_HOME/webapps !!"


#sed -i "s/.*<\/tomcat-users>*/<role rolename=\"manager-gui\"\/>\n<role rolename=\"manager-script\"\/>\n<role rolename=\"manager-jmx\"\/>\n<role rolename=\"manager-status\"\/>\n<user username=\"$TOMCAT_USER\" password=\"$TOMCAT_PASSWORD\" roles=\"manager-gui,manager-script,manager-jmx,manager-status\"\/>\n<\/tomcat-users>/" $TOMCAT_HOME/conf/tomcat-users.xml

#echo "Configured access for user $TOMCAT_USER and password $TOMCAT_PASSWORD"

# 
