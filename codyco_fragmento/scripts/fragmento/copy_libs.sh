#!/bin/bash

set -e

echo "Copying Fragmento libs to Tomcat..."

cp -R /packages/fragmento/Fragmento-Tomcat7-libs/*.jar $TOMCAT_HOME/lib

rm $TOMCAT_HOME/lib/naming-resources.jar

