#!/bin/bash

#apt-get install -y postgresql pgadmin3

#touch $POSTGRES_LOG
#chown postgres $POSTGRES_LOG
#set -e

set -e

echo "Configuring Tomcat access credentials..."
$TOMCAT_SCRIPTS/configure_access.sh

echo "Configuring Tomcat on port $TOMCAT_PORT ..."
#$TOMCAT_SCRIPTS/configure_port.sh

echo "Configuring Fragmento database access for database $FRAGMENTO_DB_NAME ..."
$FRAGMENTO_SCRIPTS/configure_database_connection.sh

echo "Copying Fragmento Libs to Tomcat library" 
$FRAGMENTO_SCRIPTS/copy_libs.sh

echo "Configuring env variables in supervisord tomcat config file..."
$TOMCAT_SCRIPTS/configure_supervisor.sh

exec supervisord -n -c /supervisor/supervisord.conf
