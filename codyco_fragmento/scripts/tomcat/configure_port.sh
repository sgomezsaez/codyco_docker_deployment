#!/bin/bash

set -e

echo "Configuring tomcat to run on port $TOMCAT_PORT ..."

export JAVA_OPTS="-XX:MaxPermSize=2058m -Dport.http.nonssl=${CATALINA_BASE_PORT} -Dport.shutdown=${CATALINA_SHUTDOWN_PORT} -Dport.ajp.port=${CATALINA_AJP_PORT}"

#apt-get install -y tomcat7
#ln -s /usr/share/tomcat7/lib /var/lib/tomcat7/lib
#ln -s /var/lib/tomcat7 /srv/tomcat7

# Remove unneeded stuff
#rm -rf $TOMCAT_HOME/opt/tomcat/webapps/examples
#rm -rf /opt/tomcat/webapps/docs

# Replace 'random' with 'urandom' for quicker startups
#rm /dev/random
#ln -s /dev/urandom /dev/random

# Links for legacy stuff
#ln -s /opt/tomcat /var/lib/tomcat7
#ln -s /opt/tomcat /srv/tomcat7
