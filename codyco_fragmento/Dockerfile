#Docker file for installing Fragmento 
# Image tag -> codyco/fragmento
FROM codyco/base
MAINTAINER Santiago Gomez Saez, Michael Hahn, Johannes Wettinger

#Setting Env variables
ENV TOMCAT_PACKAGES /packages/tomcat
ENV TOMCAT_SCRIPTS /scripts/tomcat
ENV TOMCAT_USER allow
ENV TOMCAT_PASSWORD allow
ENV FRAGMENTO_DB_NAME repository
ENV TOMCAT_PORT 8050
ENV FRAGMENTO_SCRIPTS /scripts/fragmento
ENV FRAGMENTO_PACKAGES /packages/fragmento
ENV SUPERVISOR_CONFIG /supervisor
ENV TOMCAT_HOME /opt/apache-tomcat-7.0.54

ENV TOMCAT_DOWNLOAD_URL $TOMCAT_DOWNLOAD_URL
ENV AXIS2_DOWNLOAD_URL $AXIS2_DOWNLOAD_URL

#Ports to expose (change also in run_container script
ENV TOMCAT_START_PORT 8050
ENV TOMCAT_SHUTDOWN_PORT 8180
ENV TOMCAT_AJP_PORT 8210

#ENV JAVA_OPTS -XX:MaxPermSize=2058m
#ENV JAVA_HOME /usr/lib/jvm/java-1.7.0-openjdk
#ENV PATH ${PATH}:${JAVA_HOME}/bin

# Install packages
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update && apt-get -y install unzip zip 
#openjdk-7-jdk openjdk-7-jre

#RUN ln -s /usr/lib/jvm/java-1.7.0-openjdk-amd64 /usr/lib/jvm/java-1.7.0-openjdk

# Add Tomcat package
RUN mkdir -p /packages/tomcat
RUN mkdir -p /var/log/tomcat/
#ADD packages/tomcat/apache-tomcat-7.0.54.tar.gz /packages/tomcat/apache-tomcat-7.0.54
RUN wget --no-check-certificate ${TOMCAT_DOWNLOAD_URL} -O /packages/tomcat/apache-tomcat-7.0.54.tar.gz
WORKDIR /packages/tomcat/
RUN tar -xvf apache-tomcat-7.0.54.tar.gz
WORKDIR /
RUN mkdir -p /packages/axis2/
WORKDIR /packages/axis2/
#ADD packages/axis2/axis2-1.5.6-war.zip /packages/axis2/axis2-1.5.6-war
RUN wget ${AXIS2_DOWNLOAD_URL} -O /packages/axis2/axis2-1.5.6-war.zip
RUN unzip axis2-1.5.6-war.zip
#RUN unzip /packages/axis2/axis2-1.5.6-war -d /packages/axis2/
WORKDIR /

#Add fragmento packages
RUN mkdir -p /packages/fragmento
ADD packages/fragmento/Fragmento-Tomcat7-libs.zip /packages/fragmento/Fragmento-Tomcat7-libs.zip
RUN mkdir -p /packages/fragmento/Fragmento-Tomcat7-libs
RUN unzip /packages/fragmento/Fragmento-Tomcat7-libs.zip -d /packages/fragmento/Fragmento-Tomcat7-libs
ADD packages/fragmento/Repository.war /packages/fragmento/Repository.war
 
#Installing Tomcat
#RUN cp -R /packages/tomcat/apache-tomcat-7.0.54/* /packages/tomcat/apache-tomcat-7.0.54
#RUN rm -R /packages/tomcat/apache-tomcat-7.0.54/apache-tomcat-7.0.54
RUN mv /packages/tomcat/apache-tomcat-7.0.54 /opt/apache-tomcat-7.0.54

#Installing Axis2
RUN mv /packages/axis2/axis2.war /opt/apache-tomcat-7.0.54/webapps/axis2.war

#Moving Fragmento libs 
# ASK MICHAEL WHY THIS IS NOT WORKING: THE ERROR IS Caused by: java.lang.NoSuchMethodError: org.apache.naming.resources.BaseDirContext.setCacheObjectMaxSize(I)V
#        at org.apache.catalina.core.StandardContext.setResources(StandardContext.java:2562)
RUN cp -R /packages/fragmento/Fragmento-Tomcat7-libs/*.jar /opt/apache-tomcat-7.0.54/lib/

#Delete libs that cause crash on startup of tomcat
#RUN rm -f /opt/apache-tomcat-7.0.54/lib/naming-resources.jar

#Add tomcat & fragmento configuration & start scripts
RUN mkdir -p /scripts/tomcat
ADD scripts/tomcat/configure_port.sh /scripts/tomcat/configure_port.sh
ADD scripts/tomcat/configure_access.sh /scripts/tomcat/configure_access.sh
ADD scripts/tomcat/configure_supervisor.sh /scripts/tomcat/configure_supervisor.sh
ADD scripts/run.sh /scripts/run.sh
RUN chmod 755 /scripts/tomcat/*.sh
RUN chmod 755 /scripts/*.sh

RUN mkdir -p /scripts/fragmento
ADD scripts/fragmento/configure_database_connection.sh /scripts/fragmento/configure_database_connection.sh
ADD scripts/fragmento/copy_libs.sh /scripts/fragmento/copy_libs.sh
RUN chmod 755 /scripts/fragmento/*.sh

# Add supervisord configuration file
ADD config/tomcat/supervisord.tomcat.conf /supervisor/supervisord.tomcat.conf

#Adding environment variables setters for runtime
ADD scripts/set_env.sh /etc/profile.d/set_env.sh
RUN chmod 755 /etc/profile.d/set_env.sh

EXPOSE 8050
EXPOSE 8180
EXPOSE 8210
#CMD [ "/scripts/run.sh" ]
#CMD [ "/usr/bin/supervisord", "-n", "-c", "/supervisor/supervisord.conf" ]
